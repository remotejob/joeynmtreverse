export KAGGLE_CONFIG_DIR=/home/juno/kaggleremotejob
cp datasets/dataset-metadata.json.remotejob data/dataset-metadata.json
#kaggle datasets create -r zip -p data/
kaggle datasets version -r zip -p data/ -m "Updated data 0"

export KAGGLE_CONFIG_DIR=/home/juno/kagglesipvip
cp trainconf/trainsipvip.yaml data/finbot/configs/train.yaml
cp datasets/dataset-metadata.json.sipvip data/dataset-metadata.json
#kaggle datasets create -r zip -p data/
kaggle datasets version -r zip -p data/ -m "Updated data 0"

export KAGGLE_CONFIG_DIR=/home/juno/kagglealmazseo
cp trainconf/trainalmazseo.yaml data/dataset-metadata.json
cp datasets/dataset-metadata.json.almazseo data/dataset-metadata.json
#kaggle datasets create -r zip -p data/
kaggle datasets version -r zip -p data/ -m "Updated data 0"

export KAGGLE_CONFIG_DIR=/home/juno/kaggleipotecafi
cp trainconf/trainipotecafi.yaml data/finbot/configs/train.yaml
cp datasets/dataset-metadata.json.ipotecafi data/dataset-metadata.json
#kaggle datasets create -r zip -p data/
kaggle datasets version -r zip -p data/ -m "Updated data 0"

export KAGGLE_CONFIG_DIR=/home/juno/kagglekagkagdevprod
cp trainconf/trainkagkagdevprod.yaml data/finbot/configs/train.yaml
cp datasets/dataset-metadata.json.kagkagdevprod data/dataset-metadata.json
#kaggle datasets create -r zip -p data/
kaggle datasets version -r zip -p data/ -m "Updated data 0"

unset KAGGLE_CONFIG_DIR
cp trainconf/trainaleksandermazurov.yaml data/finbot/configs/train.yaml
cp datasets/dataset-metadata.json.alesandermazurov data/dataset-metadata.json
#kaggle datasets create -r zip -p data/
kaggle datasets version -r zip -p data/ -m "Updated data 0"
# dataroot = 'data/finbot/'
# DATAROOT=data/finbot/ python botserver/runasgi.py  DEVELOPMENT
# DATAROOT=/root/data/finbot/ python botserver/runasgi.py  PROD
# 

import os
import sys
import logging



dataroot = os.environ.get('DATAROOT')


sys.path.insert(0, dataroot + "joeynmtnew")
# sys.path.insert(0, dataroot + "sacrebleu")
# sys.path.insert(0, dataroot + "portalocker")

from fastapi import FastAPI
import uvicorn
from pydantic import BaseModel

# from flask import Flask, request, jsonify
from torchtext.data import Dataset, Field
from torch import Tensor
import torch
from joeynmt.constants import UNK_TOKEN, PAD_TOKEN, EOS_TOKEN
from joeynmt.vocabulary import Vocabulary
from joeynmt.model import build_model
from joeynmt.batch import Batch
from joeynmt.helpers import log_data_info, load_config, log_cfg, \
    store_attention_plots, load_checkpoint, make_model_dir, \
    make_logger, set_seed, symlink_update, ConfigurationError
from joeynmt.model import Model
from joeynmt.prediction import validate_on_data
from joeynmt.loss import XentLoss
from joeynmt.data import load_data, make_data_iter, MonoDataset
from joeynmt.builders import build_optimizer, build_scheduler, \
    build_gradient_clipper
import sentencepiece as spm
import numpy as np
from typing import List, Optional

from voikko import libvoikko



PORT = 5000


def validate_on_data(model: Model, data: Dataset,
                     batch_size: int,
                     use_cuda: bool, max_output_length: int,
                     level: str, eval_metric: Optional[str],
                     loss_function: torch.nn.Module = None,
                     beam_size: int = 0, beam_alpha: int = -1,
                     batch_type: str = "sentence"
                     ) \
        -> (float, float, float, List[str], List[List[str]], List[str],
            List[str], List[List[str]], List[np.array]):

    valid_iter = make_data_iter(
        dataset=data, batch_size=batch_size, batch_type=batch_type,
        shuffle=False, train=False)
    valid_sources_raw = [s for s in data.src]
    pad_index = model.src_vocab.stoi[PAD_TOKEN]
    # disable dropout
    model.eval()
    # don't track gradients during validation
    with torch.no_grad():
        all_outputs = []
        valid_attention_scores = []
        total_loss = 0
        total_ntokens = 0
        total_nseqs = 0
        for valid_batch in iter(valid_iter):
            # run as during training to get validation loss (e.g. xent)

            batch = Batch(valid_batch, pad_index, use_cuda=use_cuda)
            # sort batch now by src length and keep track of order
            sort_reverse_index = batch.sort_by_src_lengths()

            # run as during training with teacher forcing
            if loss_function is not None and batch.trg is not None:
                batch_loss = model.get_loss_for_batch(
                    batch, loss_function=loss_function)
                total_loss += batch_loss
                total_ntokens += batch.ntokens
                total_nseqs += batch.nseqs

            # run as during inference to produce translations
            output, attention_scores = model.run_batch(
                batch=batch, beam_size=beam_size, beam_alpha=beam_alpha,
                max_output_length=max_output_length)

            # sort outputs back to original order
            all_outputs.extend(output[sort_reverse_index])
            valid_attention_scores.extend(
                attention_scores[sort_reverse_index]
                if attention_scores is not None else [])

        assert len(all_outputs) == len(data)

        if loss_function is not None and total_ntokens > 0:
            # total validation loss
            valid_loss = total_loss
            # exponent of token-level negative log prob
            valid_ppl = torch.exp(total_loss / total_ntokens)
        else:
            valid_loss = -1
            valid_ppl = -1

        # decode back to symbols
        decoded_valid = model.trg_vocab.arrays_to_sentences(
            arrays=all_outputs, cut_at_eos=True)

        # evaluate with metric on full dataset
        join_char = " " if level in ["word", "bpe"] else ""
        valid_sources = [join_char.join(s) for s in data.src]
        valid_references = [join_char.join(t) for t in data.trg]
        valid_hypotheses = [join_char.join(t) for t in decoded_valid]

        # if references are given, evaluate against them
        if valid_references:
            assert len(valid_hypotheses) == len(valid_references)

            current_valid_score = 0
            if eval_metric.lower() == 'bleu':
                # this version does not use any tokenization
                current_valid_score = bleu(valid_hypotheses, valid_references)
            elif eval_metric.lower() == 'chrf':
                current_valid_score = chrf(valid_hypotheses, valid_references)
            elif eval_metric.lower() == 'token_accuracy':
                current_valid_score = token_accuracy(
                    valid_hypotheses, valid_references, level=level)
            elif eval_metric.lower() == 'sequence_accuracy':
                current_valid_score = sequence_accuracy(
                    valid_hypotheses, valid_references)
        else:
            current_valid_score = -1

    return current_valid_score, valid_loss, valid_ppl, valid_sources, \
        valid_sources_raw, valid_references, valid_hypotheses, \
        decoded_valid, valid_attention_scores


def convertbpe(sp, bpephrase: str):

    strlist = bpephrase.split()
    intlist = []
    for strl in strlist:
        if strl != "<unk>":
            intlist.append(int(strl))
    return sp.DecodeIds(intlist)


def convertstrtobpe(sp, phrase: str):

    bpephrase = sp.EncodeAsIds(phrase)

    strlist = []
    for intl in bpephrase:
        strlist.append(str(intl))

    return ' '.join(strlist)


def translate(cfg_file, sp, model, line: str, ckpt: str = None, output_path: str = None) -> None:

    def _load_line_as_data(line):
        """ Create a dataset from one line via a temporary file. """
        # write src input to temporary file
        tmp_name = "tmp"
        tmp_suffix = ".src"
        tmp_filename = tmp_name+tmp_suffix
        with open(tmp_filename, "w") as tmp_file:
            tmp_file.write("{}\n".format(line))

        test_data = MonoDataset(path=tmp_name, ext=tmp_suffix,
                                field=src_field)

        # remove temporary file
        if os.path.exists(tmp_filename):
            os.remove(tmp_filename)

        return test_data

    def _translate_data(test_data):
        """ Translates given dataset, using parameters from outer scope. """
        # pylint: disable=unused-variable
        score, loss, ppl, sources, sources_raw, references, hypotheses, \
            hypotheses_raw, attention_scores = validate_on_data(
                model, data=test_data, batch_size=batch_size, level=level,
                max_output_length=max_output_length, eval_metric="",
                use_cuda=use_cuda, loss_function=None, beam_size=beam_size,
                beam_alpha=beam_alpha)
        return hypotheses

    # whether to use beam search for decoding, 0: greedy decoding
    if "testing" in cfg.keys():
        beam_size = cfg["testing"].get("beam_size", 0)
        beam_alpha = cfg["testing"].get("alpha", -1)
    else:
        beam_size = 0
        beam_alpha = -1

    linebpe = convertstrtobpe(sp, line)
    test_data = _load_line_as_data(line=linebpe)
    hypotheses = _translate_data(test_data)
    ansphrase = convertbpe(sp, hypotheses[0])
    return ansphrase


print('done!\nlaunching the server...')

sp = spm.SentencePieceProcessor()
sp.Load(dataroot + "sentencepiece/m.model")

cfg = load_config(dataroot + 'configs/serverprod.yaml')

batch_size = cfg["training"].get("batch_size", 1)
level = cfg["data"]["level"]
max_output_length = cfg["training"].get("max_output_length", None)

use_cuda = cfg["training"].get("use_cuda")

src_vocab = Vocabulary(file=dataroot+cfg['data'].get('src_vocab'))
trg_vocab = Vocabulary(file=dataroot+cfg['data'].get('trg_vocab'))

data_cfg = cfg["data"]
# level = data_cfg["level"]
lowercase = cfg["data"].get('lowercase')


def tok_fun(s): return list(s) if level == "char" else s.split()


src_field = Field(init_token=None, eos_token=EOS_TOKEN,
                  pad_token=PAD_TOKEN, tokenize=tok_fun,
                  batch_first=True, lower=lowercase,
                  unk_token=UNK_TOKEN,
                  include_lengths=True)
src_field.vocab = src_vocab

model_checkpoint = load_checkpoint(
    dataroot + cfg['training'].get('load_model'), use_cuda=use_cuda)

# build model and load parameters into it
model = build_model(cfg["model"], src_vocab=src_vocab, trg_vocab=trg_vocab)
model.load_state_dict(model_checkpoint["model_state"])

v = libvoikko.Voikko(u"fi")


class Itemask(BaseModel):
    ask: str

app = FastAPI()

@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.post("/translator/translate")
def predict(item: Itemask):
 
    ask = item.ask

    print('ASK',ask)

    askspl = ask.split()
    askoutlist = []
    for word in askspl:
        print(word)
        voikko_dict = v.analyze(word)
        if voikko_dict:
            word_baseform = voikko_dict[0]['BASEFORM'].lower()
        else:
            word_baseform = word.lower()   
        
        if word_baseform != "":
            askoutlist.append(word_baseform)

    askout = ' '.join(askoutlist)
    print("askout",askout.strip() )

    tran = translate(dataroot + 'configs/serverprod.yaml', sp, model, askout.strip())                              
 
    return {'Answer':tran}


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=PORT,log_level="info", reload=False)



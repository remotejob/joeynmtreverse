import os
import sys
import logging

sys.path.insert(0, "data/finbot/joeynmt")
sys.path.insert(0, "data/finbot/sacrebleu")
sys.path.insert(0, "data/finbot/portalocker")

from typing import List, Optional
import numpy as np
import sentencepiece as spm
from joeynmt.builders import build_optimizer, build_scheduler, \
    build_gradient_clipper
from joeynmt.data import load_data, make_data_iter, MonoDataset
from joeynmt.loss import XentLoss
from joeynmt.prediction import validate_on_data
from joeynmt.model import Model
from joeynmt.helpers import log_data_info, load_config, log_cfg, \
    store_attention_plots, load_checkpoint, make_model_dir, \
    make_logger, set_seed, symlink_update, ConfigurationError
from joeynmt.batch import Batch
from joeynmt.model import build_model
from joeynmt.vocabulary import Vocabulary
from joeynmt.constants import UNK_TOKEN, PAD_TOKEN, EOS_TOKEN
import torch
from torch import Tensor
from torch.utils.tensorboard import SummaryWriter
from torchtext.data import Dataset, Field
from flask import Flask, request, jsonify

PORT = 5000

def validate_on_data(model: Model, data: Dataset,
                     batch_size: int,
                     use_cuda: bool, max_output_length: int,
                     level: str, eval_metric: Optional[str],
                     loss_function: torch.nn.Module = None,
                     beam_size: int = 0, beam_alpha: int = -1,
                     batch_type: str = "sentence"
                     ) \
        -> (float, float, float, List[str], List[List[str]], List[str],
            List[str], List[List[str]], List[np.array]):
    """
    Generate translations for the given data.
    If `loss_function` is not None and references are given,
    also compute the loss.

    :param model: model module
    :param data: dataset for validation
    :param batch_size: validation batch size
    :param use_cuda: if True, use CUDA
    :param max_output_length: maximum length for generated hypotheses
    :param level: segmentation level, one of "char", "bpe", "word"
    :param eval_metric: evaluation metric, e.g. "bleu"
    :param loss_function: loss function that computes a scalar loss
        for given inputs and targets
    :param beam_size: beam size for validation.
        If 0 then greedy decoding (default).
    :param beam_alpha: beam search alpha for length penalty,
        disabled if set to -1 (default).
    :param batch_type: validation batch type (sentence or token)

    :return:
        - current_valid_score: current validation score [eval_metric],
        - valid_loss: validation loss,
        - valid_ppl:, validation perplexity,
        - valid_sources: validation sources,
        - valid_sources_raw: raw validation sources (before post-processing),
        - valid_references: validation references,
        - valid_hypotheses: validation_hypotheses,
        - decoded_valid: raw validation hypotheses (before post-processing),
        - valid_attention_scores: attention scores for validation hypotheses
    """
    valid_iter = make_data_iter(
        dataset=data, batch_size=batch_size, batch_type=batch_type,
        shuffle=False, train=False)
    valid_sources_raw = [s for s in data.src]
    pad_index = model.src_vocab.stoi[PAD_TOKEN]
    # disable dropout
    model.eval()
    # don't track gradients during validation
    with torch.no_grad():
        all_outputs = []
        valid_attention_scores = []
        total_loss = 0
        total_ntokens = 0
        total_nseqs = 0
        for valid_batch in iter(valid_iter):
            # run as during training to get validation loss (e.g. xent)

            batch = Batch(valid_batch, pad_index, use_cuda=use_cuda)
            # sort batch now by src length and keep track of order
            sort_reverse_index = batch.sort_by_src_lengths()

            # run as during training with teacher forcing
            if loss_function is not None and batch.trg is not None:
                batch_loss = model.get_loss_for_batch(
                    batch, loss_function=loss_function)
                total_loss += batch_loss
                total_ntokens += batch.ntokens
                total_nseqs += batch.nseqs

            # run as during inference to produce translations
            output, attention_scores = model.run_batch(
                batch=batch, beam_size=beam_size, beam_alpha=beam_alpha,
                max_output_length=max_output_length)

            # sort outputs back to original order
            all_outputs.extend(output[sort_reverse_index])
            valid_attention_scores.extend(
                attention_scores[sort_reverse_index]
                if attention_scores is not None else [])

        assert len(all_outputs) == len(data)

        if loss_function is not None and total_ntokens > 0:
            # total validation loss
            valid_loss = total_loss
            # exponent of token-level negative log prob
            valid_ppl = torch.exp(total_loss / total_ntokens)
        else:
            valid_loss = -1
            valid_ppl = -1

        # decode back to symbols
        decoded_valid = model.trg_vocab.arrays_to_sentences(
            arrays=all_outputs, cut_at_eos=True)

        # evaluate with metric on full dataset
        join_char = " " if level in ["word", "bpe"] else ""
        valid_sources = [join_char.join(s) for s in data.src]
        valid_references = [join_char.join(t) for t in data.trg]
        valid_hypotheses = [join_char.join(t) for t in decoded_valid]

        # if references are given, evaluate against them
        if valid_references:
            assert len(valid_hypotheses) == len(valid_references)

            current_valid_score = 0
            if eval_metric.lower() == 'bleu':
                # this version does not use any tokenization
                current_valid_score = bleu(valid_hypotheses, valid_references)
            elif eval_metric.lower() == 'chrf':
                current_valid_score = chrf(valid_hypotheses, valid_references)
            elif eval_metric.lower() == 'token_accuracy':
                current_valid_score = token_accuracy(
                    valid_hypotheses, valid_references, level=level)
            elif eval_metric.lower() == 'sequence_accuracy':
                current_valid_score = sequence_accuracy(
                    valid_hypotheses, valid_references)
        else:
            current_valid_score = -1

    return current_valid_score, valid_loss, valid_ppl, valid_sources, \
        valid_sources_raw, valid_references, valid_hypotheses, \
        decoded_valid, valid_attention_scores


def convertbpe(sp, bpephrase: str):

    strlist = bpephrase.split()
    intlist = []
    for strl in strlist:
        intlist.append(int(strl))
    return sp.DecodeIds(intlist)


def convertstrtobpe(sp, phrase: str):

    bpephrase = sp.EncodeAsIds(phrase)

    strlist = []
    for intl in bpephrase:
        strlist.append(str(intl))

    return ' '.join(strlist)


def translate(cfg_file, sp, model, line: str, ckpt: str, output_path: str = None) -> None:

    def _load_line_as_data(line):
        """ Create a dataset from one line via a temporary file. """
        # write src input to temporary file
        tmp_name = "tmp"
        tmp_suffix = ".src"
        tmp_filename = tmp_name+tmp_suffix
        with open(tmp_filename, "w") as tmp_file:
            tmp_file.write("{}\n".format(line))

        test_data = MonoDataset(path=tmp_name, ext=tmp_suffix,
                                field=src_field)

        # remove temporary file
        if os.path.exists(tmp_filename):
            os.remove(tmp_filename)

        return test_data

    def _translate_data(test_data):
        """ Translates given dataset, using parameters from outer scope. """
        # pylint: disable=unused-variable
        score, loss, ppl, sources, sources_raw, references, hypotheses, \
            hypotheses_raw, attention_scores = validate_on_data(
                model, data=test_data, batch_size=batch_size, level=level,
                max_output_length=max_output_length, eval_metric="",
                use_cuda=use_cuda, loss_function=None, beam_size=beam_size,
                beam_alpha=beam_alpha)
        print('loss:',loss,'score',score,'ppl',ppl,'sources',sources,'sources_raw',sources_raw,'references',references,'hypotheses',hypotheses,'hypotheses_raw',hypotheses_raw,'attention_scores',attention_scores)        
        return hypotheses

    # whether to use beam search for decoding, 0: greedy decoding
    if "testing" in cfg.keys():
        beam_size = cfg["testing"].get("beam_size", 0)
        beam_alpha = cfg["testing"].get("alpha", -1)
    else:
        beam_size = 0
        beam_alpha = -1

    linebpe = convertstrtobpe(sp, line)
    test_data = _load_line_as_data(line=linebpe)
    hypotheses = _translate_data(test_data)
    ansphrase = convertbpe(sp, hypotheses[0])
    return ansphrase


print('done!\nlaunching the server...')

rootpath = '/exwindoz/home/juno/repos/gitlab.com/remotejob/joeynmtreverse'
sp = spm.SentencePieceProcessor()
sp.Load(rootpath + "/data/finbot/sentencepiece/m.model")

cfg = load_config(rootpath + '/data/finbot/configs/server.yaml')

batch_size = cfg["training"].get("batch_size", 1)
use_cuda = cfg["training"].get("use_cuda", False)
level = cfg["data"]["level"]
max_output_length = cfg["training"].get("max_output_length", None)

use_cuda = cfg["training"].get("use_cuda", False)

src_vocab_file = cfg["data"].get(
    "src_vocab", cfg["training"]["model_dir"] + "/src_vocab.txt")
trg_vocab_file = cfg["data"].get(
    "trg_vocab", cfg["training"]["model_dir"] + "/trg_vocab.txt")
src_vocab = Vocabulary(file=src_vocab_file)
trg_vocab = Vocabulary(file=trg_vocab_file)

data_cfg = cfg["data"]
# level = data_cfg["level"]
lowercase = data_cfg["lowercase"]


def tok_fun(s): return list(s) if level == "char" else s.split()


src_field = Field(init_token=None, eos_token=EOS_TOKEN,
                  pad_token=PAD_TOKEN, tokenize=tok_fun,
                  batch_first=True, lower=lowercase,
                  unk_token=UNK_TOKEN,
                  include_lengths=True)
src_field.vocab = src_vocab

# load model state from disk
model_checkpoint = load_checkpoint(
    rootpath + '/data/finbot/models/645000.ckpt', use_cuda=use_cuda)

# build model and load parameters into it
model = build_model(cfg["model"], src_vocab=src_vocab, trg_vocab=trg_vocab)
model.load_state_dict(model_checkpoint["model_state"])

# set flask params
app = Flask(__name__)
@app.route("/")
def hello():
    return "Image classification example\n"


@app.route('/translator/translate', methods=['POST'])
def predict(rootpath=rootpath, sp=sp):
    request_json = request.get_json()

    ask = request_json.get('ask')
    tran = translate(rootpath + '/data/finbot/configs/server.yaml', sp, model, ask, rootpath +
                     '/data/finbot/models/250000.ckpt', rootpath + '/data/finbot/data/testbpeall.ans')
    return jsonify(ans=tran)


if __name__ == '__main__':
    app.run(host="0.0.0.0", debug=True, port=PORT)

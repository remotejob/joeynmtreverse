# joeynmtreverse

# joeynmt
https://github.com/joeynmt/joeynmt

#For data elab
https://medium.com/sciforce/data-cleaning-and-preprocessing-for-beginners-25748ee00743

for production
https://towardsdatascience.com/how-to-deploy-a-machine-learning-model-dc51200fe8cf

lemm
https://github.com/mikahama/uralicNLP

other:
https://github.com/hugovk/everyfinnishword
https://github.com/flammie/omorfi
https://github.com/mikahama/murre !
https://github.com/jmyrberg/finnish-word-embeddings !
https://github.com/voikko/voikko-sklearn ?
https://github.com/TurkuNLP/FinBERT  !!

https://github.com/adai183/ULMFiT_es  Get articles from wiki

https://towardsdatascience.com/teaching-lstms-to-play-god-1a11c7fe7f37  by CHAR generate



kaggle datasets create -r zip -p data/

kaggle datasets version -r zip -p data/ -m "Updated data 0"

kaggle kernels push -p train/
kaggle kernels status alesandermazurov/joeynmtdell01



rm -rf output/*
kaggle kernels output alesandermazurov/joeynmtdell01 -p output/
#cp  output/models/transformer_reverse/src_vocab.txt data/finbot/vocab
#cp  output/models/transformer_reverse/trg_vocab.txt data/finbot/vocab

export mod=430000
export dic=/exwindoz/home/juno/gowork/src/gitlab.com/remotejob/mltwiiter/

rm -rf data/finbot/models/*
cp output/models/transformer_reverse/${mod}.ckpt data/finbot/models/

spm_decode  --input_format=id --model=${dic}m.model < data/finbot/data/testbpeall.ask > data/finbot/data/testnobpeall.ask
spm_decode  --input_format=id --model=${dic}m.model < output/models/transformer_reverse/00${mod}.hyps.test > output/models/transformer_reverse/00${mod}.hyps.test.nobpe
spm_decode  --input_format=id --model=${dic}m.model < /tmp/output.txt > output/models/transformer_reverse/00${mod}.hyps.test.nobpe
paste -d"\t" data/finbot/data/testnobpeall.ask output/models/transformer_reverse/00${mod}.hyps.test.nobpe > test.txt
sed -i G test.txt
sed -i 's/\t/\n/' test.txt
sed -i 's/^\s*$/--------------/' test.txt 

spm_decode  --input_format=id --model=${dic}m.model < data/finbot/data/devbpeall.ask > data/finbot/data/devnobpeall.ask
spm_decode  --input_format=id --model=${dic}m.model < output/models/transformer_reverse/00${mod}.hyps.dev > output/models/transformer_reverse/00${mod}.hyps.dev.nobpe
paste -d"\t" data/finbot/data/devnobpeall.ask output/models/transformer_reverse/00${mod}.hyps.dev.nobpe > dev.txt
sed -i G dev.txt
sed -i 's/\t/\n/' dev.txt
sed -i 's/^\s*$/--------------/' dev.txt


cp output/my_model/trg_vocab.txt data/finbot/vocab
cp output/my_model/src_vocab.txt data/finbot/vocab



cp /home/juno/gowork/src/gitlab.com/remotejob/mltwiiter/data/src-train.txt train.de.org
cp /home/juno/gowork/src/gitlab.com/remotejob/mltwiiter/data/tgt-train.txt train.en.org
cp /exwindoz/home/juno/repos/gitlab.com/remotejob/transformerml/dataoldbot/inputsentences.txt test.de

head -n 540000 train.de.org > train.de
head -n 540000 train.en.org > train.en

tail -n 35008 train.de.org > dev.de 
tail -n 35008 train.en.org > dev.en


25.07.2019

cp /exwindoz/home/juno/repos/gitlab.com/remotejob/transformerml/data/tweedoldbot.tsv data/

cut -f1 -d$'\t' data/tweedoldbot.tsv  > data/joeynmt/test/data/finbot/train.de.org 
cut -f2 -d$'\t' data/tweedoldbot.tsv  > data/joeynmt/test/data/finbot/train.en.org

cd data/finbot/data
head -n 109000 train.ask.org > train.ask
head -n 109000 train.ans.org > train.ans

tail -n 599 train.ask.org > dev.ask
tail -n 599 train.ans.org > dev.ans

cut -f1 -d$'\t' /exwindoz/home/juno/gowork/src/gitlab.com/remotejob/mltwiiter/data/forjoeynmt2907.tsv  > data/finbot/data/train.ask.org 
cut -f2 -d$'\t' /exwindoz/home/juno/gowork/src/gitlab.com/remotejob/mltwiiter/data/forjoeynmt2907.tsv  > data/finbot/data/train.ans.org

wc data/finbot/data/train.ask.org data/finbot/data/train.ans.org
head -n 110000 data/finbot/data/train.ask.org > data/finbot/data/train.ask.nobpe
head -n 110000 data/finbot/data/train.ans.org > data/finbot/data/train.ans.nobpe

tail -n 728 data/finbot/data/train.ask.org > data/finbot/data/dev.ask.nobpe
tail -n 728 data/finbot/data/train.ans.org > data/finbot/data/dev.ans.nobpe

python src/bpeconverter.py
wc data/finbot/data/trainbpe.ask data/finbot/data/trainbpe.ans
wc data/finbot/data/devbpe.ask data/finbot/data/devbpe.ans





python src/bpedecoder.py
-------------------------------

paste -d"\t" data/finbot/data/test.ask.nobpe output/my_model/00072500.hyps.test.nobpe > test.txt
sed -i G test.txt
sed -i 's/\t/\n/' test.txt
sed -i 's/^\s*$/--------------/' test.txt 

paste -d"\t" data/finbot/data/dev.ask.nobpe output/my_model/00072500.hyps.dev.nobpe > dev.txt
sed -i G dev.txt
sed -i 's/\t/\n/' dev.txt
sed -i 's/^\s*$/--------------/' dev.txt



ALLTWEED
29.07.2019
wc /exwindoz/home/juno/gowork/src/gitlab.com/remotejob/mltwiiter/data/src-train.txt /exwindoz/home/juno/gowork/src/gitlab.com/remotejob/mltwiiter/data/tgt-train.txt

head -n 1110000 /exwindoz/home/juno/gowork/src/gitlab.com/remotejob/mltwiiter/data/src-train.txt > data/finbot/data/trainnobpe.ask
head -n 1110000 /exwindoz/home/juno/gowork/src/gitlab.com/remotejob/mltwiiter/data/tgt-train.txt > data/finbot/data/trainnobpe.ans


tail -n 8368 /exwindoz/home/juno/gowork/src/gitlab.com/remotejob/mltwiiter/data/src-train.txt > data/finbot/data/devnobpe.ask
tail -n 8368 /exwindoz/home/juno/gowork/src/gitlab.com/remotejob/mltwiiter/data/tgt-train.txt > data/finbot/data/devnobpe.ans

cp /exwindoz/home/juno/repos/gitlab.com/remotejob/joeynmt/data/finbot/data/test.ask.nobpe data/finbot/data/testnobpe.ask



spm_train --input=data/finbot/data/testnobpe.ask,data/finbot/data/trainnobpe.ask,data/finbot/data/trainnobpe.ans,data/finbot/data/devnobpe.ask,data/finbot/data/devnobpe.ans --model_prefix=m --vocab_size=10000

spm_train --input=tmp --model_prefix=m --vocab_size=10000 --num_threads=32 --input_sentence_size=100000 --shuffle_input_sentence=true

cut -f1 -d$'\t' m.vocab > data/finbot/data/finbotbpe.vocab

for i in {3..10000}
do
   echo $i >>tmp
done

<unk>
<pad>
<s>
</s>
3

spm_encode  --output_format=id --model=m.model < data/finbot/data/trainnobpe.ask  > data/finbot/data/trainbpe.ask
spm_encode  --output_format=id --model=m.model < data/finbot/data/trainnobpe.ans > data/finbot/data/trainbpe.ans
spm_encode  --output_format=id --model=m.model < data/finbot/data/devnobpe.ask > data/finbot/data/devbpe.ask
spm_encode  --output_format=id --model=m.model < data/finbot/data/devnobpe.ans > data/finbot/data/devbpe.ans
spm_encode  --output_format=id --model=m.model < data/finbot/data/testnobpe.ask  > data/finbot/data/testbpe.ask
cp data/finbot/data/testbpe.ask data/finbot/data/testbpe.ans

echo "191 96 3 874 25 52 325 133 21 530 17 249 354 351 64 53 23 13 92 80" | spm_decode --model=m.model --input_format=id


#oldbot + twitter

wc /home/juno/gowork/src/gitlab.com/remotejob/mltwiiter/data/forjoeynmt2907.tsv #110728

cut -f1 -d$'\t' /exwindoz/home/juno/gowork/src/gitlab.com/remotejob/mltwiiter/data/forjoeynmt2907.tsv  > data/finbot/data/trainnobpe.ask.org
cut -f2 -d$'\t' /exwindoz/home/juno/gowork/src/gitlab.com/remotejob/mltwiiter/data/forjoeynmt2907.tsv  > data/finbot/data/trainnobpe.ans.org
wc data/finbot/data/trainnobpe.ask.org data/finbot/data/trainnobpe.ans.org

head -n 110000 data/finbot/data/trainnobpe.ask.org > data/finbot/data/trainnobpe.ask
head -n 110000 data/finbot/data/trainnobpe.ans.org > data/finbot/data/trainnobpe.ans
tail -n 728 data/finbot/data/trainnobpe.ask.org > data/finbot/data/devnobpe.ask
tail -n 728 data/finbot/data/trainnobpe.ans.org > data/finbot/data/devnobpe.ans



python src/bpeconverterall.py
wc dataall/finbot/data/trainbpe.ask dataall/finbot/data/trainbpe.ans
wc dataall/finbot/data/devbpe.ask dataall/finbot/data/devbpe.ans
rm dataall/finbot/data/*nobpe*




#kaggle datasets create -r tar -p dataall/

kaggle datasets version -r zip -p dataall/ -m "Updated data 0"

kaggle kernels push -p trainall/
kaggle kernels status sipvip/joeynmtall



rm -rf outputall/*
kaggle kernels output sipvip/joeynmtall -p outputall/
rm outputall/my_model/*.pdf

cp  outputall/my_model/51000.ckpt dataall/finbot/models/

cp outputall/my_model/trg_vocab.txt dataall/finbot/vocab
cp outputall/my_model/src_vocab.txt dataall/finbot/vocab



python src/bpedecoderall.py
-------------------------------

paste -d"\t" dataall/finbot/data/testnobpe.ask outputall/my_model/00025000.hyps.test.nobpe > testall.txt
sed -i G testall.txt
sed -i 's/\t/\n/' testall.txt
sed -i 's/^\s*$/--------------/' testall.txt 

#paste -d"\t" dataall/finbot/data/dev.ask.nobpe outputall/my_model/00072500.hyps.dev.nobpe > devall.txt
#sed -i G devall.txt
#sed -i 's/\t/\n/' devall.txt
#sed -i 's/^\s*$/--------------/' devall.txt



05.08.2019 ALL not clean
cat /home/juno/repos/github.com/TurkuNLP/Turku-neural-parser-pipeline/seq2seqfinbot.txt /home/juno/gowork/src/gitlab.com/remotejob/mltwiiter/data/cleanoldbot2.tsv >tmp

spm_train --input=tmp --model_prefix=m --vocab_size=10000 --num_threads=32 --input_sentence_size=100000 --shuffle_input_sentence=true

wc tmp #1203614

shuf tmp > tmp.tmp
mv tmp.tmp tmp


cut -f1 -d$'\t' tmp > data/finbot/data/trainnobpe.ask.org
cut -f2 -d$'\t' tmp > data/finbot/data/trainnobpe.ans.org
wc data/finbot/data/trainnobpe.ask.org data/finbot/data/trainnobpe.ans.org

head -n 1200000 data/finbot/data/trainnobpe.ask.org > data/finbot/data/trainnobpe.ask
head -n 1200000 data/finbot/data/trainnobpe.ans.org > data/finbot/data/trainnobpe.ans
tail -n 3614 data/finbot/data/trainnobpe.ask.org > data/finbot/data/devnobpe.ask
tail -n 3614 data/finbot/data/trainnobpe.ans.org > data/finbot/data/devnobpe.ans

#rm data/finbot/data/*nobpe*


12.08.2019
rm data/finbot/data/*train*
rm data/finbot/data/*dev*


spm_train --input=/home/juno/repos/github.com/TurkuNLP/Turku-neural-parser-pipeline/seq2seqfinbot.txt --model_prefix=m --vocab_size=10000 --num_threads=32 --input_sentence_size=100000 --shuffle_input_sentence=true

cut -f1 -d$'\t' /home/juno/repos/github.com/TurkuNLP/Turku-neural-parser-pipeline/seq2seqfinbottrain.tsv >/tmp/tmpnobpe.ask
spm_encode  --output_format=id --model=m.model < /tmp/tmpnobpe.ask  > data/finbot/data/trainbpe.ask
cut -f2 -d$'\t' /home/juno/repos/github.com/TurkuNLP/Turku-neural-parser-pipeline/seq2seqfinbottrain.tsv >/tmp/tmpnobpe.ans
spm_encode  --output_format=id --model=m.model < /tmp/tmpnobpe.ans  > data/finbot/data/trainbpe.ans

cut -f1 -d$'\t' /home/juno/repos/github.com/TurkuNLP/Turku-neural-parser-pipeline/seq2seqfinbotdev.tsv >/tmp/tmpnobpe.ask
spm_encode  --output_format=id --model=m.model < /tmp/tmpnobpe.ask  > data/finbot/data/devbpe.ask
cut -f2 -d$'\t' /home/juno/repos/github.com/TurkuNLP/Turku-neural-parser-pipeline/seq2seqfinbotdev.tsv >/tmp/tmpnobpe.ans
spm_encode  --output_format=id --model=m.model < /tmp/tmpnobpe.ans  > data/finbot/data/devbpe.ans

spm_encode  --output_format=id --model=m.model < data/finbot/data/testnobpe.ask >data/finbot/data/testbpe.ask
cp data/finbot/data/testbpe.ask data/finbot/data/testbpe.ans



#paste -d"\t" /tmp/tmpbpe.ask /tmp/tmpbpe.ans > data/finbot/data/trainbpe.tsv

15.08.2019

cat data/finbot/data/devbpeall.ask | awk 'NR%10==1' > data/finbot/data/serverbpe.ask
kaggle kernels push -p botserver/
kaggle kernels status sipvip/joeynmtserver
kaggle kernels output sipvip/joeynmtserver -p output/


#server result
spm_decode  --input_format=id --model=${dic}m.model < data/finbot/data/serverbpe.ask > output/servernobpe.ask
spm_decode  --input_format=id --model=${dic}m.model < output/serverbpe.ans > output/servernobpe.ans
paste -d"\t" output/servernobpe.ask  output/servernobpe.ans > serverresult.txt
sed -i G serverresult.txt
sed -i 's/\t/\n/' serverresult.txt
sed -i 's/^\s*$/--------------/' serverresult.txt


21.08.2019
tar zcfv joeynmtdata.tar.gz data/


23.08.2019
DATAROOT=data/finbot/ python botserver/runasgi.py 


25.08.2019 Deployment

tar  zcfv joeynmtreversedata.tar.gz --exclude=data/finbot/data data/
tar  zcfv joeynmtreversedata.tar.gz data/
cd data/
tar  zcfv joeynmtreversedata.tar.gz finbot/
scp joeynmtreversedata.tar.gz root@159.203.67.26:data/ && rm joeynmtreversedata.tar.gz
systemctl stop asgi.service
#untar on remote 




python data/finbot/joeynmt/scripts/plot_validations.py output/models/transformer_reverse --plot_values bleu PPL --output_path my_plot.pdf


28.10.2019

cp ../joeynmtreverse03/data/finbot/configs/serverprod.yaml data/finbot/configs/
cp ../joeynmtreverse03/data/finbot/configs/train.yaml data/finbot/configs/
cp ../joeynmtreverse03/data/finbot/models/240000.ckpt data/finbot/models/
cp ../joeynmtreverse03/data/finbot/sentencepiece/m.model data/finbot/sentencepiece/
cp ../joeynmtreverse03/data/finbot/vocab/* data/finbot/vocab


12.12.2019
#edit train.yaml
conda activate kaggle
kaggle datasets version -r zip -p data/ -m "Updated data 0"

kaggle kernels push -p train/
kaggle kernels status alesandermazurov/joeynmtdell01


rm -rf output/*
export KAGGLE_CONFIG_DIR=/home/juno/kagglesipvip
kaggle kernels output sipvip/joeynmtreverse -p output/

export KAGGLE_CONFIG_DIR=/home/juno/kagglekagkagdevprod
kaggle kernels output kagkagdevprod/joeynmtreverse -p output/

unset KAGGLE_CONFIG_DIR
kaggle kernels output alesandermazurov/joeynmtreverse -p output/

##sudo chmod -R o+rwx output/models/
#cp  output/models/transformer_reverse/src_vocab.txt data/finbot/vocab
#cp  output/models/transformer_reverse/trg_vocab.txt data/finbot/vocab


export mod=430000
export dic=/exwindoz/home/juno/gowork/src/gitlab.com/remotejob/mltwiiter/

rm -rf data/finbot/models/*
cp output/models/transformer_reverse/${mod}.ckpt data/finbot/models/

#edit train.yaml

deploy:

cd data/
tar  zcfv joeynmtreversedata.tar.gz finbot/
scp joeynmtreversedata.tar.gz root@159.203.67.26:data/ && rm joeynmtreversedata.tar.gz
systemctl stop asgi.service
#untar on remote 


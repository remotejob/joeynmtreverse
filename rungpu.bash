export KAGGLE_CONFIG_DIR=/home/juno/kaggleremotejob
cp kernelgpu/kernel-metadata.json.remotejob train/kernel-metadata.json
kaggle kernels push -p train/

export KAGGLE_CONFIG_DIR=/home/juno/kagglesipvip
cp kernelgpu/kernel-metadata.json.sipvip train/kernel-metadata.json
kaggle kernels push -p train/

export KAGGLE_CONFIG_DIR=/home/juno/kagglealmazseo
cp kernelgpu/kernel-metadata.json.almazseo train/kernel-metadata.json
kaggle kernels push -p train/

export KAGGLE_CONFIG_DIR=/home/juno/kaggleipotecafi
cp kernelgpu/kernel-metadata.json.ipotecafi train/kernel-metadata.json
kaggle kernels push -p train/

export KAGGLE_CONFIG_DIR=/home/juno/kagglekagkagdevprod
cp kernelgpu/kernel-metadata.json.kagkagdevprod train/kernel-metadata.json
kaggle kernels push -p train/

unset KAGGLE_CONFIG_DIR
cp kernelgpu/kernel-metadata.json.alesandermazurov train/kernel-metadata.json
kaggle kernels push -p train/